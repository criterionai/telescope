import logging
import cv2

from pydantic import Field
from pydantic.typing import Literal
from typing import List, Dict
import tensorflow as tf
import numpy as np

from brevettiai.platform import Job, JobSettings, ModelArchive
from brevettiai.io.h5_metadata import save_model
from brevettiai.data.data_generator import StratifiedSampler
from brevettiai.data.sample_tools import BrevettiDatasetSamples
from brevettiai.data.sample_integrity import SampleSplit
from brevettiai.data.image import ImageLoader, AnnotationLoader, ImageAugmenter, CropResizeProcessor
from brevettiai.data.image.annotation_parser import expand_samples_with_annotations
from brevettiai.data.image.utils import tile2d
from brevettiai.interfaces.pivot import export_pivot_table
from brevettiai.model.factory.mobilenetv2_backbone import lightning_segmentation_backbone, thunder_segmentation_backbone
from brevettiai.model.factory.lenet_backbone import lenet_backbone
from brevettiai.model.factory.lraspp import LRASPP2SegmentationHead
from brevettiai.model.factory.segmentation import SegmentationModel

log = logging.getLogger(__name__)

model_catalogue = {
    "Lightning": {"backbone_factory": lightning_segmentation_backbone(), "head_factory": LRASPP2SegmentationHead(),
                  "resize_output": True},
    "Thunder": {"backbone_factory": thunder_segmentation_backbone(), "head_factory": LRASPP2SegmentationHead(),
                "resize_output": True},
    "LeNet": {"backbone_factory": lenet_backbone(), "head_factory":
        LRASPP2SegmentationHead(avg_pool_kernel=(3, 3), avg_pool_strides=(2, 2)), "resize_output": True}
}

model_options = tuple(model_catalogue.keys())


class TelescopeSegmentationSettings(JobSettings):
    """
    Settings object for segmentation model training
    """
    type: Literal["TelescopeSegmentation"] = "TelescopeSegmentation"
    epochs: int = Field(default=15, gt=0, lt=250,
                        description="Number of epochs to train the model", advanced=False)
    classes: List[str] = Field(default_factory=list, description="List of classes to use", advanced=False)
    label_mapping: Dict[str, str] = Field(default_factory=dict, description="mapping from annotation label to class")
    batch_size: int = Field(default=4, description="Number of images per batch")
    model: Literal[model_options] = "Lightning"
    image: CropResizeProcessor = Field(default_factory=CropResizeProcessor)
    enable_augmentation: bool = Field(default=True, advanced=False)
    augmenter: ImageAugmenter = Field(default_factory=ImageAugmenter,
                                      description="""Module to perform image augmentation
https://docs.brevetti.ai/developers/tutorials/3_brevettiai_data_science_tools#image-augmentation""")


class TelescopeSegmentationJob(Job):
    """
    Job to perform a simple segmentation model
    """
    settings: TelescopeSegmentationSettings

    def run(self):
        log.info("Start Training")
        output_path = self.train()
        log.info("Training complete")
        return output_path

    def train(self):
        settings = self.settings

        # Find images with annotations
        full_set = BrevettiDatasetSamples().get_samples(self.datasets, annotations={"how": "left",
                                                                                    "prefix": "annotation_"})
        full_set = SampleSplit(seed=42).update_unassigned(full_set,
                                                          id_path=self.artifact_path("sample_identification.csv"))

        # List all annotations on images
        annotation_samples = expand_samples_with_annotations(full_set, io=self.io, key="annotation_path")
        # Drop samples with no annotations
        annotation_samples = annotation_samples.dropna()

        # append found folder names if the classes list is empty
        if not settings.classes:
            settings.classes.extend(annotation_samples.label.unique())
        else:
            annotation_samples = annotation_samples[annotation_samples.label.isin(settings.classes)]

        fields = {"label": "label",
                  "dataset_id": {"label": "Dataset id",
                                 "sort": [x.id for x in sorted(self.datasets, key=lambda x: x.name)]},
                  "purpose": "Purpose"}
        tags = self.backend.get_root_tags(self.id, self.api_key)
        export_pivot_table(self.artifact_path("pivot", dir=True), annotation_samples, fields,
                           datasets=self.datasets, tags=tags, rows=["label"], cols=["purpose"],
                           agg={"url": "first"})

        train_samples = annotation_samples[annotation_samples.purpose == 'train']
        development_samples = annotation_samples[annotation_samples.purpose == 'devel']


        # Sampling methods
        sampler = StratifiedSampler(batch_size=settings.batch_size)

        # Get example image to infer channels of dataset (grayscale / rgb)
        example_loader = ImageLoader(postprocessor=settings.image, channels=0, io=self.io)
        example_image, _ = example_loader.load(annotation_samples.path.iloc[0])

        # Mapping for data ingestion pipeline
        image_loader = ImageLoader(postprocessor=settings.image, channels=example_image.shape[-1], io=self.io)

        annotation_loader = AnnotationLoader(postprocessor=settings.image, classes=settings.classes,
                                             mapping=settings.label_mapping, io=self.io)

        # Create data generators for training and development sets
        train_ds = sampler.get(train_samples, output_structure=("img", "annotation"), shuffle=True, repeat=True,
                               seed=42).map([image_loader, annotation_loader])

        if settings.enable_augmentation:
            train_ds = train_ds.map(settings.augmenter)

        development_ds = sampler.get(development_samples, output_structure=("img", "annotation"),
                                     shuffle=True, repeat=True, seed=42)\
            .map([image_loader, annotation_loader])

        # Build model
        model_factory = SegmentationModel(**model_catalogue[settings.model], classes=settings.classes)

        model = model_factory.build(image_loader.output_shape())
        # model_factory.backbone.trainable = False

        model.compile(optimizer=tf.keras.optimizers.Adam(lr=0.01),
                      loss='binary_crossentropy')
        model_factory.model.summary()

        # Train model
        tensorboard = self.temp_path("tensorboard", self.run_id, dir=True)
        log.info(f"run following command to see tensorboard: 'tensorboard --logdir {tensorboard}'")
        callbacks = [self.get_remote_monitor(), tf.keras.callbacks.TensorBoard(log_dir=tensorboard, profile_batch=0)]
        # Filter empty callbacks
        callbacks = [c for c in callbacks if c is not None]

        model.fit(train_ds.get_dataset(),
                  epochs=settings.epochs,
                  steps_per_epoch=len(train_ds),
                  validation_data=development_ds.get_dataset().take(len(development_ds)),
                  callbacks=callbacks)

        # Serialize model and upload as artifact
        tmp_model = self.temp_path("model.h5")
        save_model(tmp_model, model, metadata=self.get_metadata())

        model_archive = ModelArchive(job=self)
        with model_archive.open_write() as writer:
            writer.add_asset("h5", "model.h5", tmp_model)
        model_archive_artifact = model_archive.upload(self)

        # Test model
        predict_batches = 16
        test_ds = sampler.get(full_set, shuffle=False, repeat=False)\
            .map([image_loader])
        imgs = np.concatenate(list(test_ds.get_dataset(structure="img").take(predict_batches)))
        yhat = model.predict(test_ds.get_dataset(structure="img").take(predict_batches))

        # Upload images as artifacts
        # Create label image to demonstrate color encoding
        label_image_location = self.artifact_path("label_image.png")
        label_image = np.eye(3).repeat(20000, 0).reshape(300, 200, 3)
        for class_index, class_name in enumerate(settings.classes[:3]):
            cv2.putText(label_image, class_name, (20, 50 + 100 * class_index), cv2.FONT_HERSHEY_SIMPLEX, 1.5, 0, 3)
        ok, buffer = cv2.imencode(".png", label_image * 255)
        self.upload_artifact(label_image_location, bytes(buffer))

        # Create tiled result image with output segmentations as color coding
        result_image_location = self.artifact_path("results.png")
        result_image = tile2d(yhat[..., :3]  * 128 + imgs, (predict_batches, settings.batch_size))
        ok, buffer = cv2.imencode(".png", result_image)
        self.io.write_file(result_image_location, bytes(buffer))

        # Add result image to html to show it on the platform model page
        self.io.write_file(self.artifact_path("results.html"), f"""
        <p> Segmentation output color coded on top of images using these color labels: </p>
        <img src="{self.backend.get_download_link(label_image_location)}" alt="Color labels of image segmentation">
        <br>
        <p> Results are shown on test samples with no annotations provided - note that other images of some physical samples may have been included in the training, though.   
        <br>
        <img src="{self.backend.get_download_link(result_image_location)}" alt="Resulting segmentation">
        """)

        return model_archive_artifact
