import logging
from brevettiai import Job
from telescope import TelescopeJob, TelescopeSegmentationJob

log = logging.getLogger(__name__)


if __name__ == "__main__":
    # initialize job from job_id / api_key in argv
    job = Job.init(log_level=logging.DEBUG, type_selector=[TelescopeJob, TelescopeSegmentationJob])
    logging.getLogger("tensorflow").setLevel(logging.ERROR)
    job.start()
