import logging

from pydantic import Field
from pydantic.typing import Literal
from typing import List
import tensorflow as tf

from brevettiai.platform import Job, JobSettings
from brevettiai.data.sample_tools import BrevettiDatasetSamples
from brevettiai.data.sample_integrity import SampleSplit
from brevettiai.data.image import ImageLoader, ImageAugmenter, CropResizeProcessor
from brevettiai.data.data_generator import StratifiedSampler, OneHotEncoder
from . import data

log = logging.getLogger(__name__)


class TelescopeSettings(JobSettings):
    type: Literal["Telescope"] = "Telescope"
    epochs: int = Field(default=30, gt=0, lt=250,
                        description="Number of epochs to train the model", advanced=False)
    classes: List[str] = Field(default_factory=list, description="List of classes to use", advanced=False)
    batch_size: int = Field(default=32, description="Number of images per batch")
    image: CropResizeProcessor = Field(default_factory=CropResizeProcessor)
    augmenter: ImageAugmenter = Field(default_factory=ImageAugmenter)
    sampler: StratifiedSampler = Field(default_factory=StratifiedSampler)


class TelescopeJob(Job):
    settings: TelescopeSettings

    def run(self):
        log.info("Start Training")
        output_path = self.train()
        log.info("Training complete")
        return output_path

    def train(self):
        settings = self.settings

        full_set = BrevettiDatasetSamples(classes=settings.classes).get_samples(self.datasets, annotations=False)
        full_set = SampleSplit().update_unassigned(full_set, id_path=self.artifact_path("sample_identification.csv"))

        train_samples = full_set[full_set.purpose == 'train']
        development_samples = full_set[full_set.purpose == 'devel']

        # append found folder names if the classes list is empty
        if not settings.classes:
            settings.classes.extend(full_set.folder.unique())

        # Sampling methods
        sampler = StratifiedSampler(batch_size=settings.batch_size)

        # Mapping for data ingestion pipeline
        image_loader = ImageLoader(postprocessor=settings.image, channels=3, io=self.io)
        one_hot = OneHotEncoder(classes=settings.classes, input_key="folder")

        train_ds = sampler.get(train_samples, output_structure=("img", "onehot"), shuffle=True, repeat=True)\
            .map([image_loader, settings.augmenter, one_hot])

        development_ds = sampler.get(development_samples, output_structure=("img", "onehot"), shuffle=True, repeat=True, seed=42)\
            .map([image_loader, one_hot])

        # Build model
        backbone = tf.keras.applications.EfficientNetB0(
            input_shape=image_loader.output_shape(), include_top=False, weights="imagenet"
        )
        backbone.trainable = False
        output_name = "---".join(settings.classes)
        model = tf.keras.Sequential([
            backbone,
            tf.keras.layers.GlobalMaxPooling2D(),
            tf.keras.layers.Dropout(0.05),
            tf.keras.layers.Dense(len(settings.classes), activation='softmax', name=output_name)
        ])
        model.compile(optimizer=tf.keras.optimizers.Adam(lr=0.01),
                      loss='categorical_crossentropy',
                      metrics=['accuracy'])
        model.summary()

        # Train model
        tensorboard = self.temp_path("tensorboard", self.run_id, dir=True)
        callbacks = [self.get_remote_monitor(), tf.keras.callbacks.TensorBoard(log_dir=tensorboard, profile_batch=0)]
        # Filter empty callbacks
        callbacks = [c for c in callbacks if c is not None]

        model.fit(train_ds.get_dataset(),
                  epochs=settings.epochs,
                  steps_per_epoch=len(train_ds),
                  validation_data=development_ds.get_dataset().take(len(development_ds)),
                  callbacks=callbacks)

        # Test
        loss, acc = model.evaluate(development_ds.get_dataset(), steps=len(development_ds),
                                   verbose=1)
        log.info("Accuracy: " + str(acc))

        # Export
        saved_model_dir = self.temp_path("export", "saved_model", dir=True)
        example_image = next(train_ds.get_dataset_numpy(structure="img", batch=False))

        model.save(saved_model_dir, overwrite=True, include_optimizer=False,
                   signatures=data.get_serving_receiver(model, shape=example_image.shape[:2],
                                                        scale=1, offset=0))

        output_path = data.package_saved_model(saved_model_dir)
        # Upload
        artifact_path = self.artifact_path("saved_model.tar.gz")
        self.io.copy(output_path, artifact_path)

        return artifact_path
