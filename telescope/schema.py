import os
import json
import brevettiai.interfaces.vue_schema_utils as vue
from telescope import TelescopeSettings, TelescopeSegmentationSettings

if __name__ == "__main__":
    import argparse

    schema_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "settings-schema.json")

    parser = argparse.ArgumentParser()
    parser.add_argument('--schema_path', help='Output location of schema', default=schema_path)
    parser.add_argument('--type', help='type of schema to export [Classification/Segmentation]',
                        default=schema_path, required=True)
    args = parser.parse_args()

    if args.type.lower() == "classification":
        print(f"Generating classification application schema to '{args.schema_path}'")
        schema = TelescopeSettings.platform_schema()
        schema.presets["NeurIPS vials demo"] = json.loads(TelescopeSettings().json())
        vue.generate_application_schema(schema, path=args.schema_path)
    elif args.type.lower() == "segmentation":
        print(f"Generating segmentation application schema to '{args.schema_path}'")
        schema = TelescopeSegmentationSettings.platform_schema()
        schema.presets["NeurIPS vials demo"] = json.loads(TelescopeSegmentationSettings(
            epochs=30,
            classes=['Cap', 'Thread', 'Neck', 'Container'],
            enable_augmentation=False,
        ).json())
        vue.generate_application_schema(schema, path=args.schema_path)
    else:
        raise AttributeError(f"type '{args.type}' not known; should be Classification or Segmentation")

