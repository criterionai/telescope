import unittest
import os
import re
from importlib_metadata import version


class TestApplicationSchema(unittest.TestCase):

    def test_check_docker_base_image_version(self):
        """ Test Dockerfile uses same tensorflow version as the tester"""
        with open(os.path.normpath(os.path.join(os.path.dirname(os.path.abspath(__file__)), "../Dockerfile"))) as fp:
            docker = fp.read()
        tensorflow_docker = re.findall(r"tensorflow/tensorflow:(\d\.\d\.\d.*)\s", docker)[-1]
        tensorflow_package = version('tensorflow')
        assert tensorflow_docker.split("-")[0] == tensorflow_package, \
            f"Docker file tensorflow:{tensorflow_docker} " \
            f"does not match test environment tensorflow version {tensorflow_package}"


if __name__ == '__main__':
    unittest.main()
