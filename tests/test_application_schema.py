import unittest

from telescope import TelescopeJob, TelescopeSettings


class TestApplicationSchema(unittest.TestCase):

    minimal_settings = {

    }

    def test_generate_application_schema(self):
        schema = TelescopeSettings.platform_schema().schema
        assert isinstance(schema, dict)
        assert "fields" in schema
        assert isinstance(schema["fields"], (list, tuple))

    def test_initialize_application(self):
        job = TelescopeJob(
            name="Test model",
            id="id",
            job_dir="",
            settings=self.minimal_settings,
            tags=[],
            datasets=[],
        )
        assert job.settings.epochs > 0
        assert job.settings.sampler.batch_size > 0


if __name__ == '__main__':
    unittest.main()
