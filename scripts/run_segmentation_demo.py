from brevettiai.platform import PlatformAPI
from telescope import TelescopeSegmentationJob, TelescopeSegmentationSettings
from brevettiai.data.image import CropResizeProcessor

if __name__ == "__main__":
    # Log in to the platform
    web = PlatformAPI(remember_me=False)

    # NeurIPS demo vials train
    datasets = web.get_dataset(name="NeurIPS demo vials train")
    settings = TelescopeSegmentationSettings(
        epochs=50,
        model="Lightning",
        # image=CropResizeProcessor(output_height=224, output_width=224),
        classes=['Cap', 'Thread', 'Neck', 'Container'],
        enable_augmentation=True,
    )

    model = web.create_model(f"{web.user['firstName']}: NeurIPS segmentation demo - {settings.model}",
                             datasets=datasets, settings=settings)

    job = web.initialize_training(model, job_type=TelescopeSegmentationJob)
    job.start()
