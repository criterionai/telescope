# Telescope

Telescope is a Brevetti AI sample application capable of training a simple image classification model,
or a simple image segmentation model.

Get started by cloning the repository and
downloading the python [poetry dependency manager](https://python-poetry.org/docs/master/#installing-with-the-official-installer)


```commandline
git clone https://bitbucket.org/criterionai/telescope.git
curl -sSL https://install.python-poetry.org | python3 -
```

Install package into a virtual environment

```commandline
cd telescope
poetry install
```

Run segmentation demo
```commandline
poetry run python .\scripts\run_segmentation_demo.py
```

---
**NOTE for Windows users**

Using poetry in a Windows may result in path lengths > 260 (the Windows default maximum). Possible fixes:

* Change location of poetry virtual environment ``` poetry config virtualenvs.path C:\my_shorter_venv_path ```
* [Enable long paths in Windows 10 or later](https://docs.microsoft.com/en-us/windows/win32/fileio/maximum-file-path-limitation?tabs=cmd)

---
