ARG base_image=tensorflow/tensorflow:2.8.0-gpu
FROM $base_image

WORKDIR /brevetti

COPY requirements.txt requirements.txt
RUN pip --no-cache-dir install -r requirements.txt

COPY dist/ dist/
RUN pip --no-cache-dir install dist/*.whl

ARG build_id=-1
ENV BUILD_ID=$build_id

# Sets up the entry point to invoke the trainer.
ENTRYPOINT ["python", "-m", "telescope.train"]